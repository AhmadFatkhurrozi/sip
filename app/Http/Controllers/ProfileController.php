<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Pemohon;
use App\Models\Users;
use Auth, Redirect, Validator, Session, Hash;

class ProfileController extends Controller
{
    private $title = "My Profil";
    private $menuActive = "profile";
    private $submnActive = "";

    public function index()
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        $data['user'] = Users::join('pemohon as p', 'p.user_id', 'users.id')->find(Auth::getUser()->id);
        
        return view($this->menuActive.'.main', $data)->with('data', $this->data);
    }

    public function update(Request $request)
    {   
        $id = $request->id;

        $profile = Users::find($id);
        $profile->username     = $request->username;
        $profile->email        = $request->email;
        if ($request->password != null) {
            $profile->password     = Hash::make($request->password);
        }
        $profile->nama         = $request->nama;
        $profile->level        = 3;
        $profile->bagian_id    = 0;
        $profile->save();

        $pemohon = Pemohon::where('user_id', $id)->first();
        $pemohon->nama = $request->nama;
        $pemohon->nik = $request->nik;
        $pemohon->jenis_kelamin = $request->gender;
        $pemohon->no_telp = $request->no_telp;
        $pemohon->status = $request->pekerjaan;
        $pemohon->alamat = $request->alamat;
        $pemohon->save();

        if($pemohon){
            return redirect(route('profile'));
        }
    }
}
