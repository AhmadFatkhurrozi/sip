<?php



namespace App\Http\Controllers\Site;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Libraries\CompresImage;

use App\Models\Users;

use App\Models\Bagian;

use App\Models\Permohonan;

use App\Models\MasterLayanan;

use Auth, Redirect, Validator, DB, Session, PDF;



class PengajuanController  extends Controller

{

    private $title = "Pengajuan";



    public function main(Request $request)

    {

        $data['judul'] = $this->title;

        $data['pengajuan'] = Permohonan::where('user_id', Auth::id())->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->get();

        $data['layanan'] = MasterLayanan::join('master_bagian as mb', 'mb.id_bagian', 'master_pelayanan.bagian_id')->get();

        return view('pengajuan.main', $data);

    }



    public function form(Request $request)

    {

        $data['judul'] = "Tambah Pengajuan";

        $data['layanan'] = MasterLayanan::join('master_bagian as mb', 'mb.id_bagian', 'master_pelayanan.bagian_id')->get();

        return view('pengajuan.form', $data);

    }



    public function add(Request $request)

    {

        $validator=Validator::make($request->all(),[

          'pelayanan_id' => 'required',
          
          "berkas" => "required|mimes:pdf|max:10000",

          "lampiran" => "mimes:pdf|max:10000",

        ]);



        if ($validator->fails()) {

            Session::flash('pesan','Format file harus PDF');


            return redirect()->back()->withInput()->withErrors($validator->errors());

        }else{

            if (!empty($request->berkas)) {

                $ukuranFile = filesize($request->berkas);

                $ext_dok         = $request->berkas->getClientOriginalExtension();

                $filename        = "uploads/pengajuan/".date('YmdHis').'.'.$ext_dok;

                $temp_dok        = 'uploads/pengajuan/';

                $proses          = $request->berkas->move($temp_dok, $filename);

            }

            

            if (!empty($request->lampiran)) {

                $ukuranFile1 = filesize($request->lampiran);

                $ext_dok1         = $request->lampiran->getClientOriginalExtension();

                $filename1        = "uploads/pengajuan/lamp".date('YmdHis').'.'.$ext_dok1;

                $temp_dok1        = 'uploads/pengajuan/';

                $proses1          = $request->lampiran->move($temp_dok1, $filename1);

            }



            $cekPeng = Permohonan::count();

            $noBukti = $cekPeng + 1;

            

            $pemohon = new Permohonan;

            if (!empty($request->berkas)) {

                $pemohon->berkas = $filename;

            }

            if (!empty($request->lampiran)) {

                $pemohon->lampiran = $filename1;

            }

            $pemohon->pelayanan_id = $request->pelayanan_id;

            $pemohon->nomor_bukti = date('Ymd').$noBukti;

            $pemohon->user_id = Auth::getUser()->id;

            $pemohon->status = 'pending';

            $pemohon->tanggal = date('Y-m-d');

            $pemohon->catatan = "";

            $pemohon->save();



            Session::flash('pesan','Pengajuan Berhasil');

            return Redirect::route('Pengajuan');

        }

    }



    public function reupload(Request $request)

    {

        $data['id'] = $request->id;

        $data['data'] = Permohonan::join('users as u', 'u.id', 'permohonan.user_id')->join('pemohon as p', 'u.id', 'p.user_id')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->find($request->id);

        $content = view('pengajuan.reupload',$data)->render();

        return ['status' => 'success', 'content' => $content];

    }



    public function saveReupload(Request $request)
    {
        $validator=Validator::make($request->all(),[
          
          "berkasBaru" => "required|mimes:pdf|max:10000",

          "lampiran" => "mimes:pdf|max:10000",

        ]);



        if ($validator->fails()) {

            Session::flash('pesan','Format file harus PDF');


            return redirect()->back()->withInput()->withErrors($validator->errors());

        }else{


            if (!empty($request->berkasBaru)) {

                $ukuranFile = filesize($request->berkasBaru);

                $ext_dok         = $request->berkasBaru->getClientOriginalExtension();

                $filename        = "uploads/pengajuan/".date('YmdHis').'.'.$ext_dok;

                $temp_dok        = 'uploads/pengajuan/';

                $proses          = $request->berkasBaru->move($temp_dok, $filename);

            }

            

            if (!empty($request->lampiran)) {

                $ukuranFile1 = filesize($request->lampiran);

                $ext_dok1         = $request->lampiran->getClientOriginalExtension();

                $filename1        = "uploads/pengajuan/lamp".date('YmdHis').'.'.$ext_dok1;

                $temp_dok1        = 'uploads/pengajuan/';

                $proses1          = $request->lampiran->move($temp_dok1, $filename1);

            }



        

            $pemohon = Permohonan::where('id_permohonan', $request->id)->first();

            if (!empty($request->berkasBaru)) {

                $pemohon->berkas = $filename;

            }

            if (!empty($request->lampiran)) {

                $pemohon->lampiran = $filename1;

            }

            $pemohon->status = 'pending';

            $pemohon->tanggal = date('Y-m-d');

            $pemohon->catatan = "";

            $pemohon->save();

            

            Session::flash('pesan','Berhasil Upload Ulang Berkas');

            return Redirect::route('Pengajuan');
        }

    }



    public function batalPengajuan(Request $request)

    {

      $cekPermohonan = Permohonan::find($request->id);

      if (!empty($cekPermohonan)) {

        $dt = $cekPermohonan;

        $dt->status = "Dibatalkan";

        $dt->catatan = "";

        $dt->save();



        if ($dt) {

          return ['status'=>'success'];

        }else{

          return ['status'=>'error'];

        }

      }

    }

    public function cetakNoBukti(Request $request)
    {
        $data['data'] = Permohonan::join('users as u', 'u.id', 'permohonan.user_id')->join('pemohon as p', 'u.id', 'p.user_id')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->find($request->id);

        $data['judul'] = "Nomor Bukti : ".$data['data']->nomor_bukti;

        $pdf = PDF::loadView('pengajuan.cetak', $data)
                  ->setPaper('a4', 'portrait');

        return $pdf->stream('NomorBUkti-'.$data['data']->nomor_bukti.'.pdf');
    }

}

