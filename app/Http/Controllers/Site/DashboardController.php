<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session, Auth;
class DashboardController extends Controller
{
  function main(){
    $data['judul'] = "Dashboard";
    return view('home.main', $data);
  }

}