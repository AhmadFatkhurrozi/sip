<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\CompresImage;
use App\Models\Users;
use App\Models\Bagian;
use App\Models\MasterLayanan;
use Auth, Redirect, Validator, DB;

class PelayananController  extends Controller
{
    private $title = "Daftar Layanan & Persyaratan";

    public function main(Request $request)
    {
        $data['judul'] = $this->title;
        $data['layanan'] = MasterLayanan::join('master_bagian as mb', 'mb.id_bagian', 'master_pelayanan.bagian_id')->get();
        return view('layanan.main', $data);
    }
}
