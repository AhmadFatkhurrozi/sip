<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\User;
use App\Models\Pemohon;
use Auth,Redirect,Hash,Session, Validator;

class LoginController extends Controller
{
  function main(){
    return view('home.login');
  }

  function registrasi(){
    return view('home.registrasi');
  }

  function doLogin(Request $request)
  {
    // return $request->all();
    $username = $request->username;
    $password = $request->password;
    $user = User::whereRaw("username='$username' OR email='$username'")->first();
    if(!empty($user)){
      if(Hash::check($password,$user->password)){
        $credential = [
        'email' => $user->email,
        'password' =>$password,
        ];
        Auth::attempt($credential);
        return Redirect::to('/');
      }else{
        Session::flash('pesan','Wrong Password');
        return Redirect::route('login');
      }
    }else{
      Session::flash('pesan','you must to Registration');
      return Redirect::route('login');
    }
  }

  public function log_admin()
  {
    return view('admin.log_admin');
  }

  public function dolog_admin(Request $request)
  {
    $username = $request->username;
    $password = $request->password;
    $user = User::whereRaw("username='$username' OR email='$username'")->first();
    if(!empty($user)){
      if(Hash::check($password,$user->password)){
        $credential = [
        'email' => $user->email,
        'password' =>$password,
        ];
        Auth::attempt($credential);
        return Redirect::to('admin');
      }else{
        return Redirect::route('log_admin')->with('title', 'Whoops')->with('message', 'Wrong Password')->with('type', 'error');
      }
    }else{
      return Redirect::route('log_admin')->with('title', 'you must to Registration')->with('message', 'You do not have an account')->with('type', 'error');
    }
  }
  public function logout(){
    \Auth::logout();
    return redirect('/'); // ini untuk redirect setelah logout
  }

  public function logout_admin(){
    \Auth::logout();
    return redirect('/admin'); // ini untuk redirect setelah logout
  }

  public function store(Request $request)
  {
      $rules = array(
        'email'     => 'required',
        'username'  => 'required',
        'nama'      => 'required',
        'password'  => 'required',
      );
     
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );

      $validator    = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $cekUsername = Users::where('username', $request->username)->first();
        if (empty($cekUsername)) {
          $cekEmail = Users::where('email', $request->email)->first();
          if (empty($cekEmail)) {
            $dtUser               = (!empty($request->id)) ? Users::find($request->id) : new Users;
            $dtUser->username     = $request->username;
            $dtUser->email        = $request->email;
            $dtUser->password     = Hash::make($request->password);
            $dtUser->nama         = $request->nama;
            $dtUser->level        = 3;
            $dtUser->bagian_id    = 0;
            $dtUser->save();

            if ($dtUser) {
              $idUser = $dtUser->id;
              $validator=Validator::make($request->all(),[
                  'gender' => 'required',
                  'no_telp' => 'required',
                  'pekerjaan' => 'required',
                  'alamat' => 'required',
                  'nik' => 'required',
              ]);

              if ($validator->fails()) {
                  return redirect()->back()->withInput()->withErrors($validator->errors());
              }else{
                $pemohon = Pemohon::create([
                  'nama' => $request->nama,
                  'nik' => $request->nik,
                  'jenis_kelamin' => $request->gender,
                  'no_telp' => $request->no_telp,
                  'status' => $request->pekerjaan,
                  'alamat' => $request->alamat,
                  'user_id' => $idUser,
                ]);

                Session::flash('pesan','Pendaftaran Berhasil');
                return Redirect::route('login');
              }

            }else{
              $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Email Sudah Digunakan, Silahkan menggunakan Email Lain !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Username Sudah Digunakan, Silahkan menggunakan Username Lain !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }    
  }
