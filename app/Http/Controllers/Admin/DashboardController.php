<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Permohonan;
use Auth,Redirect,Hash,Session;


class DashboardController extends Controller
{

  function main(){
    if (Auth::getUser()->level == 1) {
      $data['permohonan'] = Permohonan::count();
    }else{
      $data['permohonan'] = Permohonan::join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->join('users as u', 'u.id', 'permohonan.user_id')->where('mp.bagian_id', Auth::getUser()->bagian_id)->count();
    }
    $data['judul'] = 'Dashboard';
    return view('admin.dashboard.main',$data);
  }
}
