<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\CompresImage;
use App\Models\Users;
use App\Models\Bagian;
use App\Models\MasterLayanan;
use Auth, Redirect, Validator, DB;

class LayananController  extends Controller
{
    private $title = "Data Layanan";

    public function main(Request $request)
    {
        $data['judul'] = $this->title;
        return view('admin.layanan.main', $data);
    }

    public function datagrid(Request $request)
    {
        $data = MasterLayanan::getJson($request);
        return response()->json($data);
    }

    public function form(Request $request)
    {
      $data['bagian'] = Bagian::all();
      $data['data'] = (!empty($request->id) ? MasterLayanan::find($request->id) : "");
      $content = view('admin.layanan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
      $rules = array(
        'nama_pelayanan' => 'required',
        'bagian_id'     => 'required',
        'syarat'     => 'required',
      );
     
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );

      $validator    = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
          $dtUser  = (!empty($request->id)) ? MasterLayanan::find($request->id) : new MasterLayanan;
          $dtUser->nama_pelayanan   = $request->nama_pelayanan;
          $dtUser->bagian_id        = $request->bagian_id;
          $dtUser->syarat        = $request->syarat;

          if (!empty($request->berkas)) {
            $ukuranFile = filesize($request->berkas);
            $ext_dok         = $request->berkas->getClientOriginalExtension();
            $filename        = "uploads/layanan/".date('YmdHis').'.'.$ext_dok;
            $temp_dok        = 'uploads/layanan/';
            $proses          = $request->berkas->move($temp_dok, $filename);
            $dtUser->formulir = $filename;
          }

          $dtUser->save();

          if ($dtUser) {
            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
          }

        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
    public function show(Request $request)
    {
      $id = $request->id;
      $data['data'] = MasterLayanan::join('master_bagian as mb', 'mb.id_bagian', 'master_pelayanan.bagian_id')->where('id_pelayanan', $id)->first();

      if (!empty($data['data'])) {
        $content = view('admin.layanan.show',$data)->render();

        return ['status'=>'success','content'=>$content, 'data'=>$data];
      }else{
        return ['status'=>'failed','content'=>''];
      }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $data['User'] = MasterLayanan::find($id);

        if (!empty($data['User'])) {
            $content = view('admin.layanan.update',$data)->render();

            return ['status'=>'success','content'=>$content, 'data'=>$data];
        }else{
            return ['status'=>'failed','content'=>''];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $do_delete = MasterLayanan::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
