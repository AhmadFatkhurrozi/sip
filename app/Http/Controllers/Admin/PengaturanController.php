<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\CompresImage;
use Auth, Redirect, Validator, DB;

class PengaturanController  extends Controller
{
    private $title = "Pengaturan";

    public function main(Request $request)
    {
        $data['identity'] = DB::table('pengaturan')->where('id', 1)->first();
        $data['judul'] = $this->title;
        return view('admin.pengaturan.main', $data);
    }

    public function simpan(Request $request)
    { 
      if (!empty($request->alur)) {
          // $ext_foto = $request->alur->getClientOriginalExtension();
          $ext_foto = 'jpg';
          $filename = 'alur.'.$ext_foto;
          $temp_foto = 'uploads/';
          $proses = $request->alur->move($temp_foto, $filename);
      }

      if (!empty($request->header)) {
          // $ext_foto2 = $request->header->getClientOriginalExtension();
          $ext_foto2 = 'jpg';
          $filename2 = 'header.'.$ext_foto2;
          $temp_foto2 = 'uploads/';
          $proses2 = $request->header->move($temp_foto, $filename2);
      }

      $data = [
        'alur' => $filename,
        'header' => $filename2,
      ];

      DB::table('pengaturan')->where('id', 1)->update($data);

      return Redirect::route('Pengaturan')->with('title', 'Success !')->with('message', 'Identitas Successfully Updated !!')->with('type', 'success');
    }
}
