<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\CompresImage;
use App\Models\Users;
use App\Models\Bagian;
use App\Models\MasterLayanan;
use Auth, Redirect, Validator, DB;

class BagianController  extends Controller
{
    private $title = "Data Bagian";

    public function main(Request $request)
    {
        $data['judul'] = $this->title;
        return view('admin.bagian.main', $data);
    }

    public function datagrid(Request $request)
    {
        $data = Bagian::getJson($request);
        return response()->json($data);
    }

    public function form(Request $request)
    {
      $data['data'] = (!empty($request->id) ? Bagian::find($request->id) : "");
      $content = view('admin.bagian.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
      $rules = array(
        'nama_bagian' => 'required',
      );
     
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );

      $validator    = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $dtUser               = (!empty($request->id)) ? Bagian::find($request->id) : new Bagian;
        $dtUser->nama_bagian     = $request->nama_bagian;
        $dtUser->save();

        if ($dtUser) {
          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }

        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
    public function show(Request $request)
    {
      $id = $request->id;
      $data['data'] = Bagian::find($id);

      if (!empty($data['data'])) {
        $content = view('admin.bagian.show',$data)->render();

        return ['status'=>'success','content'=>$content, 'data'=>$data];
      }else{
        return ['status'=>'failed','content'=>''];
      }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $data['User'] = Bagian::find($id);

        if (!empty($data['User'])) {
            $content = view('admin.bagian.update',$data)->render();

            return ['status'=>'success','content'=>$content, 'data'=>$data];
        }else{
            return ['status'=>'failed','content'=>''];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $do_delete = Bagian::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
