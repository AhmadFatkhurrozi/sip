<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Libraries\CompresImage;

use App\Models\Users;

use App\Models\Bagian;

use App\Models\Permohonan;

use App\Models\MasterLayanan;

use Auth, Redirect, Validator, DB, Session, PDF;



class PengajuanController  extends Controller

{

    private $title = "Data Pengajuan";



    public function main(Request $request)

    {

        $data['judul'] = $this->title;

        return view('admin.pengajuan.main', $data);

    }

    public function Laporan(Request $request)
    {
         $data['judul'] = "Laporan";

        return view('admin.pengajuan.laporan', $data);
    }



    public function datagrid(Request $request)

    {
        // return $request->all();

        $data = Permohonan::getJson($request);

        return response()->json($data);

    }

    public function cetakLaporan(Request $request)
    {
        $bulan = $request->bulan.'-01';

        if (isset($request->bulan)) {
            $data['judul'] = "Bulan ".date('m-Y', strtotime($bulan));
            $data['data'] = Permohonan::selectRaw('permohonan.*, mp.*, u.*, mb.nama_bagian')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')
                    ->join('users as u', 'u.id', 'permohonan.user_id')
                    ->join('master_bagian as mb', 'mb.id_bagian', 'mp.bagian_id')->where('permohonan.tanggal', 'like', '%'.$request->bulan.'%')->get();
        }else{
            $data['judul'] = "";
            $data['data'] = Permohonan::selectRaw('permohonan.*, mp.*, u.*, mb.nama_bagian')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')
                    ->join('users as u', 'u.id', 'permohonan.user_id')
                    ->join('master_bagian as mb', 'mb.id_bagian', 'mp.bagian_id')->get();
        }

        $pdf = PDF::loadView('admin.pengajuan.cetak', $data)
                  ->setPaper('a4', 'portrait');

        return $pdf->stream('Laporan'.date('dmYHis').'.pdf');
    }



    public function formValidasi(Request $request)

    {

      $data['id'] = $request->id;

      $data['data'] = Permohonan::join('users as u', 'u.id', 'permohonan.user_id')->join('pemohon as p', 'u.id', 'p.user_id')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->find($request->id);

      $content = view('admin.pengajuan.formValidasi',$data)->render();

      return ['status' => 'success', 'content' => $content];

    }

    

    public function uploadSertifikat(Request $request)

    {

      $data['id'] = $request->id;

      $data['data'] = Permohonan::join('users as u', 'u.id', 'permohonan.user_id')->join('pemohon as p', 'u.id', 'p.user_id')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->find($request->id);

      $content = view('admin.pengajuan.uploadSertifikat',$data)->render();

      return ['status' => 'success', 'content' => $content];

    }

    

    public function addSertifikat(Request $request){

        if (!empty($request->berkas)) {

            $ukuranFile = filesize($request->berkas);

            $ext_dok    = $request->berkas->getClientOriginalExtension();

            $filename   = "uploads/pengajuan/s".date('YmdHis').'.'.$ext_dok;

            $temp_dok   = 'uploads/pengajuan/';

            $proses     = $request->berkas->move($temp_dok, $filename);

        }



    

        $pemohon = Permohonan::where('id_permohonan', $request->id)->update([

          'sertifikat' => $filename

        ]);



        Session::flash('pesan','Berhasil Upload Sertifikat');

        return Redirect::route('AdminPengajuan');

    }



    public function show(Request $request)

    {

      $data['id'] = $request->id;

      $data['data'] = Permohonan::join('users as u', 'u.id', 'permohonan.user_id')->join('pemohon as p', 'u.id', 'p.user_id')->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->find($request->id);

      $content = view('admin.pengajuan.show',$data)->render();

      return ['status' => 'success', 'content' => $content];

    }



    public function approve(Request $request)

    {

      $cekPermohonan = Permohonan::find($request->id);

      if (!empty($cekPermohonan)) {

        $dt = $cekPermohonan;

        $dt->status = $request->status;

        $dt->catatan = $request->catatan;

        if($request->status == "Diterima"){

            $dt->tanggal_diterima = date('Y-m-d');

        }

        $dt->save();



        if ($dt) {

          return ['status'=>'success'];

        }else{

          return ['status'=>'error'];

        }

      }

    }



    public function chartBulanan(Request $request)

    {

        $tahun = date("Y");

        $title = 'Grafik Pengajuan';

        $subTitle = 'Tahun : '.$tahun;

        $dtGrafik = [];



    





        $arrSelesai = [];



        for ($i=1; $i <= 12 ; $i++) {



           if (Auth::getUser()->level == 1) {

              $selesai = Permohonan::whereYear('tanggal', $tahun)

                                ->whereMonth('tanggal', $i)

                                ->count();

            }else{

              $selesai = Permohonan::join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')->join('users as u', 'u.id', 'permohonan.user_id')->where('mp.bagian_id', Auth::getUser()->bagian_id)->whereYear('tanggal', $tahun)->whereMonth('tanggal', $i)->count();

            }

    

          



          array_push($arrSelesai, (int)$selesai);



        }



        $dtGrafik[] = [

          'no' => 1,

          'nama' => "Pengajuan",

          'jumlah' => $arrSelesai,

        ];





      return ['dtGrafik'=>$dtGrafik, 'title'=>$title, 'subTitle'=>$subTitle];

    }

}

