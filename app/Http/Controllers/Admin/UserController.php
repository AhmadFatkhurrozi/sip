<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\CompresImage;
use App\Models\Users;
use App\Models\Bagian;
use Auth, Redirect, Validator, DB;

class UserController  extends Controller
{
    private $title = "Data User";

    public function main(Request $request)
    {
        $data['judul'] = $this->title;
        return view('admin.user.main', $data);
    }

    public function datagrid(Request $request)
    {
        $data = Users::getJson($request);
        return response()->json($data);
    }

    public function form(Request $request)
    {
      $data['bagian'] = Bagian::all();
      $data['data'] = (!empty($request->id) ? Users::find($request->id) : "");
      $content = view('admin.user.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
      $rules = array(
        'email'     => 'required',
        'username'  => 'required',
        'nama'      => 'required',
        'level'     => 'required',
        'bagian_id' => 'required',
      );
     
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );

      $validator    = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $cekUsername = Users::where('username', $request->username)->first();
        if (empty($cekUsername)) {
          $cekEmail = Users::where('email', $request->email)->first();
          if (empty($cekEmail)) {
            $dtUser               = (!empty($request->id)) ? Users::find($request->id) : new Users;
            $dtUser->username     = $request->username;
            $dtUser->email        = $request->email;
            
            if(!empty($request->id)){
                if ($request->password != null) {
                    $dtUser->password     = bcrypt($request->password);
                }
            }else{
                $dtUser->password     = bcrypt($request->username);   
            }
            $dtUser->nama         = $request->nama;
            $dtUser->level        = $request->level;
            $dtUser->bagian_id    = $request->bagian_id;
            $dtUser->save();

            if ($dtUser) {
              $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
              $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Email Sudah Digunakan, Silahkan menggunakan Email Lain !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Username Sudah Digunakan, Silahkan menggunakan Username Lain !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
    public function show(Request $request)
    {
      $id = $request->id;
      $data['data'] = Users::leftjoin('master_bagian as mb', 'mb.id_bagian', 'users.bagian_id')->where('id', $id)->first();

      if (!empty($data['data'])) {
        $content = view('admin.user.show',$data)->render();

        return ['status'=>'success','content'=>$content, 'data'=>$data];
      }else{
        return ['status'=>'failed','content'=>''];
      }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $data['User'] = Users::find($id);

        if (!empty($data['User'])) {
            $content = view('admin.user.update',$data)->render();

            return ['status'=>'success','content'=>$content, 'data'=>$data];
        }else{
            return ['status'=>'failed','content'=>''];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $do_delete = Users::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
