<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pemohon;
use App\Http\Libraries\CompresImage;
use Auth, Redirect, Validator, DB;

class PemohonController  extends Controller
{
    private $title = "Data Pemohon";

    public function main(Request $request)
    {
        $data['judul'] = $this->title;
        return view('admin.pemohon.main', $data);
    }

    public function datagrid(Request $request)
    {
        $data = Pemohon::getJson($request);
        return response()->json($data);
    }

    public function show(Request $request)
    {
      $id = $request->id;
      $data['data'] = Pemohon::join('users as u', 'u.id', 'pemohon.user_id')->find($id);

      if (!empty($data['data'])) {
        $content = view('admin.pemohon.show',$data)->render();

        return ['status'=>'success','content'=>$content, 'data'=>$data];
      }else{
        return ['status'=>'failed','content'=>''];
      }
    }

}
