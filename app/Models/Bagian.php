<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Bagian extends Model
{
    protected $table='master_bagian';
    protected $primaryKey='id_bagian';
    public $timestamps=false;

    public static function getJson($input)
    {
      $table  = 'master_bagian';
      $select = '*';

      $replace_field  = [
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data;
      });
      return $data;
    }
}
