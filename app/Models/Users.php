<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Libraries\Datagrid;
use Auth;

class Users extends Authenticatable
{
    use Notifiable;
    protected $table='users';
    protected $primaryKey='id';
    public $timestamps=false;

    public static function getJson($input)
    {
      $table  = 'users';
      $select = 'users.*, mb.*';

      $replace_field  = [
        ['old_name' => 'Level', 'new_name' => 'level'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->leftjoin('master_bagian as mb', 'mb.id_bagian', 'users.bagian_id')->where('level', '!=', 3);
      });
      return $data;
    }
}
