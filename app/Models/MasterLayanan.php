<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class MasterLayanan extends Model
{
    protected $table='master_pelayanan';
    protected $primaryKey='id_pelayanan';
    public $timestamps=false;

    public static function getJson($input)
    {
      $table  = 'master_pelayanan';
      $select = 'master_pelayanan.*, mb.*';

      $replace_field  = [
        ['old_name' => 'file', 'new_name' => 'formulir'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->join('master_bagian as mb', 'mb.id_bagian', 'master_pelayanan.bagian_id');
      });
      return $data;
    }
}
