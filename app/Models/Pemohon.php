<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Pemohon extends Model
{
    protected $table='pemohon';
    protected $fillable = ['id_pemohon', 'nik', 'alamat', 'nama', 'status', 'user_id', 'no_telp', 'jenis_kelamin'];
    protected $primaryKey='id_pemohon';
    public $timestamps=false;

    public static function getJson($input)
    {
      $table  = 'pemohon';
      $select = 'pemohon.*, u.*';

      $replace_field  = [
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->join('users as u', 'u.id', 'pemohon.user_id');
      });
      return $data;
    }
}
