<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Permohonan extends Model
{
    protected $table='permohonan';
    protected $primaryKey='id_permohonan';
    protected $fillable = ['id_permohonan', 'nomor_bukti', 'user_id', 'pelayanan_id', 'status', 'tanggal', 'berkas', 'catatan'];
    public $timestamps=false;

    public static function getJson($input)
    {
      $table  = 'permohonan';
      $select = 'permohonan.*, mp.*, u.*, mb.nama_bagian';

      $replace_field  = [
        ['old_name' => 'noBukti', 'new_name' => 'nomor_bukti'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data) use ($input){
        if (Auth::getUser()->level == 1) {

          $data->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')
                    ->join('users as u', 'u.id', 'permohonan.user_id')
                    ->join('master_bagian as mb', 'mb.id_bagian', 'mp.bagian_id');

          if (!empty($input['periode'])) {
              $data->where('permohonan.tanggal', 'like', '%'.$input['periode'].'%');
          }


          return $data;
        }else{

          $data->join('master_pelayanan as mp', 'mp.id_pelayanan', 'permohonan.pelayanan_id')
                    ->join('users as u', 'u.id', 'permohonan.user_id')
                    ->join('master_bagian as mb', 'mb.id_bagian', 'mp.bagian_id')
                    ->where('mp.bagian_id', Auth::getUser()->bagian_id);

          if (!empty($input['periode'])) {
              $data->where('permohonan.tanggal', 'like', '%'.$input['periode'].'%');
          }

          return $data;
        }
      });
      return $data;
    }
}
