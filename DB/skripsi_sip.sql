-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2021 at 11:51 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_sip`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_bagian`
--

CREATE TABLE `master_bagian` (
  `id_bagian` int(11) NOT NULL,
  `nama_bagian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_bagian`
--

INSERT INTO `master_bagian` (`id_bagian`, `nama_bagian`) VALUES
(1, 'PONTREN'),
(2, 'BIMAIS');

-- --------------------------------------------------------

--
-- Table structure for table `master_pelayanan`
--

CREATE TABLE `master_pelayanan` (
  `id_pelayanan` int(11) NOT NULL,
  `nama_pelayanan` varchar(255) NOT NULL,
  `bagian_id` int(10) UNSIGNED NOT NULL,
  `syarat` text DEFAULT NULL,
  `formulir` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_pelayanan`
--

INSERT INTO `master_pelayanan` (`id_pelayanan`, `nama_pelayanan`, `bagian_id`, `syarat`, `formulir`) VALUES
(1, 'Permohonan Mendirikan Pondok Pesantren', 1, 'Isi Formulir berikut. Jika sudah, upload berkas ke menu pengajuan', 'uploads/layanan/20201202232139.docx'),
(3, 'Permohonan Pengajuan Sertifikat Pondok Pesantren', 1, 'Isi Formulir berikut. Jika sudah, upload berkas ke menu pengajuan', 'uploads/layanan/20201202232425.doc'),
(4, 'Pengajuan Mendirikan Masjid/ Musholla', 2, 'Isi Formulir berikut. Jika sudah, upload berkas ke menu pengajuan', 'uploads/layanan/20201202231504.doc'),
(5, 'Permohonan Sertifikasi Masjid', 2, 'Buat Proposal dengan template berikut. Jika sudah, upload berkas ke menu pengajuan', 'uploads/layanan/20201202231914.docx');

-- --------------------------------------------------------

--
-- Table structure for table `pemohon`
--

CREATE TABLE `pemohon` (
  `id_pemohon` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `user_id` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemohon`
--

INSERT INTO `pemohon` (`id_pemohon`, `nama`, `nik`, `jenis_kelamin`, `alamat`, `status`, `no_telp`, `user_id`) VALUES
(1, 'Pemohon 1', '1111222211112222', 'Laki-laki', 'Jombang', 'Mahasiswa', '-', '14');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `alur` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `alur`, `header`) VALUES
(1, 'alur.jpg', 'header.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `permohonan`
--

CREATE TABLE `permohonan` (
  `id_permohonan` int(11) NOT NULL,
  `nomor_bukti` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `pelayanan_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `berkas` varchar(255) NOT NULL,
  `catatan` text DEFAULT NULL,
  `tanggal_diterima` date DEFAULT NULL,
  `lampiran` varchar(255) DEFAULT NULL,
  `sertifikat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `permohonan`
--

INSERT INTO `permohonan` (`id_permohonan`, `nomor_bukti`, `user_id`, `pelayanan_id`, `status`, `tanggal`, `berkas`, `catatan`, `tanggal_diterima`, `lampiran`, `sertifikat`) VALUES
(4, '202012102', 14, 4, 'Diterima', '2021-12-22', 'uploads/pengajuan/20201222223315.pdf', NULL, '2021-02-05', NULL, 'uploads/pengajuan/s20210205215142.pdf'),
(5, '202012222', 14, 1, 'pending', '2021-02-05', 'uploads/pengajuan/20210205213729.pdf', '', NULL, NULL, NULL),
(6, '202101153', 14, 3, 'Dibatalkan', '2021-01-15', 'uploads/pengajuan/20210115230116.pdf', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `bagian_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `nama`, `password`, `level`, `remember_token`, `bagian_id`) VALUES
(1, 'admin', 'admin@gmail.com', 'Administrator', '$2y$10$EPrkprvkvk6HljBBBlvOH.5UNiDXMPQDs5UZvReOc6DkElKSw8f8O', '1', '4w7upbDRiR99Savh4YElLATAuKXBDeQukATZKbdDGY9NKnykGH9y45p8gu6A', NULL),
(7, 'stafpontren', 'staf1@gmail.com', 'Staf Pontren', '$2y$10$LEHEv7876757LfRHZREp4uPXQ.Hu79k/XNQfGaHMwW.6qu/fg7gQm', '2', 'SMPmFF2751d8gyyG1o47fH9cZ0bTv9ZWETtumohClXx3ZtoOy56rK4AnRGBn', 1),
(8, 'stafbimais', 'staf2@gmail.com', 'Staf Bimais', '$2y$10$MdPeqCK0T4rW/p0zKWbMYexIuMChMwCCmXXX4VvP8RBEPTQ1rRYae', '2', '0Yo7tqK98KknR2J41xSacoJWxpWCcQ3ulOciB8UplNYtNSfsHAEfqCVhFJaC', 2),
(14, 'pemohon', 'pemohon@gmail.com', 'Pemohon 1', '$2y$10$Ei1HdIPz02YfU7lfKSO3ruproHy5UNKeQmL3hkuC8uHECRf5HmQBO', '3', 'DjJouDk5kcQBxuFqzPJC4vTKDCb5jMPoIfyHrPxYznyso3Uf7To2QOyBPgSD', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_bagian`
--
ALTER TABLE `master_bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `master_pelayanan`
--
ALTER TABLE `master_pelayanan`
  ADD PRIMARY KEY (`id_pelayanan`);

--
-- Indexes for table `pemohon`
--
ALTER TABLE `pemohon`
  ADD PRIMARY KEY (`id_pemohon`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permohonan`
--
ALTER TABLE `permohonan`
  ADD PRIMARY KEY (`id_permohonan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_bagian`
--
ALTER TABLE `master_bagian`
  MODIFY `id_bagian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_pelayanan`
--
ALTER TABLE `master_pelayanan`
  MODIFY `id_pelayanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pemohon`
--
ALTER TABLE `pemohon`
  MODIFY `id_pemohon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permohonan`
--
ALTER TABLE `permohonan`
  MODIFY `id_permohonan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
