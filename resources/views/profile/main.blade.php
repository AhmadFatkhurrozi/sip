@extends('layout.main')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ route('updateProfil') }}">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h1 class="mt-3">Profil Saya</h1>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
        <label>Username</label>
        <input type="hidden" name="id" value="{{$user->id}}" class="form-control" >
        <input type="text" name="username" value="{{$user->username}}" class="form-control" >
      </div>

      <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" autocomplete="new-password" class="form-control password">
        <span>(kosongi jika tidak ingin mengubah password)</span>
      </div>

      <div class="form-group">
        <label>NIK</label>
        <input type="text" name="nik" value="{{$user->nik}}" class="form-control">
      </div>

      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="nama" value="{{$user->nama}}" class="form-control">
      </div>
      
      <div class="form-group">
        <label>Status Pekerjaan : </label><br>
        {{-- <label class="radio-inline mr-3">
            <input type="radio" name="pekerjaan" @if ($user->status == "PNS") checked="" @endif value="PNS"><b>PNS</b>
          </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" @if ($user->status == "Dosen") checked="" @endif value="Dosen"><b>Dosen</b>
        </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" @if ($user->status == "Mahasiswa") checked="" @endif value="Mahasiswa"><b>Mahasiswa</b>
        </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" @if ($user->status == "Umum") checked="" @endif value="Umum"><b>Umum</b>
        </label> --}}
        <input type="text" name="pekerjaan" class="form-control" value="{{$user->status}}">
      </div>
    </div>


    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
          <label>Jenis Kelamin</label>
          <select class="form-control" name="gender">
            <option @if ($user->jenis_kelamin == "Laki-laki") selected="" @endif value="Laki-laki">Laki-laki</option>
            <option @if ($user->jenis_kelamin == "Perempuan") selected="" @endif value="Perempuan">Perempuan</option>
          </select>
        </div>
    
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" value="{{$user->email}}" class="form-control">
          <small class="form-text text-muted">Gunakan Email aktif.</small>
        </div>
      
        <div class="form-group">
          <label>Nomor Telepon</label>
          <input type="text" name="no_telp" value="{{$user->no_telp}}" class="form-control">
        </div>
      
        <div class="form-group">
          <label>Alamat</label>
          <textarea class="form-control" name="alamat">{{$user->alamat}}</textarea>
        </div>

        <button type="submit" class="btn btn-success btn-xs btn-block">Update Profile</button>
    </div>

  </div>
</form>
@endsection

@section('js')
<script type="text/javascript">

</script>
@endsection