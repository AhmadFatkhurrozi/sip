<!DOCTYPE html>
<html lang="en">
<head>
  <title>SIP - Sistem Informasi Pelayanan Online</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{asset('images/logo.png')}}" sizes=""/>

  <link rel="stylesheet" href="{{asset('css/bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/beranda.css')}}">
  <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery-ui.js')}}"></script>
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
  <script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

  <style type="text/css">
    nav.navbar{
      color: #fff;
      background: #134E5E;
      background: -webkit-linear-gradient(to right, #134E5E, #71B280);
      background: linear-gradient(to right, #134E5E, #71B280);
    }
    #nav-link{
      color: #fff !important;
    }
    /*.panel-content{
      border-right: 1px solid green;
      padding-right: 20px;
    }*/
  </style>
</head>

<body>
  {{-- <div class="topHeader">
    <div class="Hlogo1"><img src="" style="float: left;margin-top: 10px;margin-left:30px" width="125px"></div>
    <div class="Hlogo2"><img src="" style="float: right;margin: 30px !important" width="60px"></div>
  </div> --}}

  <div class="col-lg-12 col-md-12" style="padding: 0px">
    <div class="cover">
    </div>
  </div>
  <div class="clearfix"></div>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand" href="{{ url('/') }}">SI - Pelayanan Online</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{url('/') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('frontPelayanan') }}">Pelayanan & Persyaratan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('Pengajuan') }}">Pengajuan</a>
        </li>
      </ul>
      <div class="float-right">
        {{-- <span>  </span> --}}
        <ul class="navbar-nav mr-auto">
          @if(!empty(Auth::id()))
          <li class="nav-item">
            <a class="nav-link" href="{{ route('profile') }}"> <i class="fa fa-user"></i> {{Auth::getUser()->username}} </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/logout')}}"> Keluar </a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link btn btn-outline-success btn-sm px-3 text-light mr-2" href="{{url('/login')}}"> Login </a>
          </li>
          <li class="nav-item">
            <a class="nav-link btn btn-success btn-sm px-3 text-light mr-2" href="{{url('/registrasi')}}"> Daftar </a>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 text-left panel-content">
        @yield('content')
      </div>

      {{-- <div class="col-lg-3 col-md-3" style="padding: 20px">
        <div class="col-lg-12 col-md-12">
          <div style="width: 100%;height: auto; text-align: left; font-weight: bold;">
            <div class="panel-group">
              <div class="panel panel-default">
                @if(!empty(Auth::id()))
                <div class="panel-body">
                  <ul class="list-unstyled">
                    <li><a href="{{url('userhome/edit_profil')}}">My Profil</a></li>
                    <li><a href="{{url('/logout')}}">Logout</a></li>
                  </ul>
                </div>
                @else
                <div class="panel-body">
                  <a class="btn btn-outline-success btn-block" href="{{url('/login')}}">Login</a></li>
                  <a class="btn btn-block btn-success" href="{{url('/registrasi')}}">Daftar</a>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div> --}}
    </div>
  </div>

  <footer class="footer">
   <div class="row">
      <div class="col-md-12 col-sm-12 my-3">
        {{-- <h2 class="text-center" id="address">Tentang</h2> --}}
        <p><b>Sistem Informasi Pelayanan Online</b> - Aplikasi untuk Melayani Pengajuan Permohonan di Kemenag Jombang</p>
        <p>Copyright 2020 @ AIDZARHUMAM</p>
      </div>
      {{-- <div class="col-md-6 col-sm-12">
        <h2 class="text-center" id="address">Alamat</h2>
        <p><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15820.625501200584!2d112.2262685!3d-7.5579228!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe2324a9eba7c84c7!2sKemenag%20Jombang!5e0!3m2!1sid!2sid!4v1606912261802!5m2!1sid!2sid" width="600" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>
      </div> --}}
     </div>
  </footer>

  <script type="text/javascript">
    @if(Session::has('pesan'))
    swal("{{ Session::get('pesan')}}")
    @endif

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    jQuery(document).ready(function() {

      var btn = $('#button');
      btn.hide();

      $(window).scroll(function() {
        if ($(window).scrollTop() > 100) {
          btn.show();
        } else {
          btn.hide();
        }
      });

      btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
      });


      $('#refresh').click(function(){
        $.ajax({
          type:'GET',
          url:'refreshcaptcha',
          success:function(data){
            $(".captcha span").html(data.captcha);
          }
        });
      });

      // CKEDITOR.replace( 'summary-ckeditor' );
      // CKEDITOR.replace( 'summary-ckeditor2' );

      function EasyPeasyParallax() {
        var scrollPos = $(document).scrollTop();
        if (scrollPos < 100) {
          $('.topHeader').attr('style','background-color:rgba(255,255,255,0)');
        } else {
          // $('.topHeader').attr('style','background-color:rgba(89, 118, 182,1)');
          $('.topHeader').attr('style','background-color:rgba(255, 255, 255,1)');
        }
      }

      $(function(){
        $(window).scroll(function() {
          EasyPeasyParallax();
        });
      });
    });

  </script>
  @yield('js')

</body>
</html>

