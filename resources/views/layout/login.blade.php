
<!DOCTYPE html>
<html>
<head>
<title>Login Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

	{{-- <link rel="stylesheet" href="{{url('css/bootstrap.css')}}"> --}}
	<link rel="stylesheet" href="{{asset('logintemp/css/style.css')}}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{url('css/sweetalert.css')}}">
	<script src="{{url('js/sweetalert.min.js')}}"></script>
	<style type="text/css">
		.background-overlay {
		    position: fixed;
		    background: linear-gradient(to bottom,#76616A,#D9BDF9,#F5ACDA);
		    background-size: 100% 100%;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    left: 0;
		    opacity: .8;
		    z-index: -5;
		}
	</style>
</head>
<body>
	<div class="background-overlay"></div>	
	<div class="container">
		@yield('content')
	</div>
	<div class="footer">
	     <p>Copyright &copy; {{date('Y')}} Kemenag Kabupaten Jombang</p>
	</div>
</body>
<script>
  @if(Session::has('message'))
  	swal('{{Session::get('title')}}','{{Session::get('message')}}','{{Session::get('type')}}');
  @endif
</script>
</html>