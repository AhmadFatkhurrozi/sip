@extends('admin.master.main')
@section('content')
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          {{$judul}}
        </div>
      </div>

     {{--  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom: 10px">
        <div class="panel-body-card bgPinkCard">
          <div class="iconCard"><i class="fa fa-file-text"></i></div>
          <div class="contenCard">
            <div></div>
            <div class="Prof"><a href="" style="color: white;text-decoration: underline;"></a></div>
          </div>
        </div>
        <a href="javascript:void(0)">
        </a>
      </div> --}}


      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom: 10px">
        <div class="panel-body-card bgOrangeCard">
          <div class="iconCard"><i class="fa fa-user"></i></div>
          <div class="contenCard">
            <div>{{\DB::table('users')->where('level', '!=', 3)->count()}}</div>
            <div class="Prof"><a href="{{ route('user') }}" style="color: white;text-decoration: underline;">Pengguna</a></div>
          </div>
        </div>
        <a href="javascript:void(0)">
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom: 10px">
        <div class="panel-body-card bgLimeCard">
          <div class="iconCard"><i class="fa fa-users"></i></div>
          <div class="contenCard">
            <div>{{\DB::table('pemohon')->count()}}</div>
            <div class="Prof"><a href="{{ route('pemohon') }}" style="color: white;text-decoration: underline;">Pemohon</a></div>
          </div>
        </div>
        <a href="javascript:void(0)">
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom: 10px">
        <div class="panel-body-card bgGreenCard">
          <div class="iconCard"><i class="fa fa-file"></i></div>
          <div class="contenCard">
            <div>{{$permohonan}}</div>
            <div class="Prof"><a href="{{ route('AdminPengajuan') }}" style="color: white;text-decoration: underline;">Pengajuan</a></div>
          </div>
        </div>
        <a href="javascript:void(0)">
        </a>
      </div>

      <div class="clearfix"></div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="container-fluid">
      {{-- <div class="text-center font-weight-bold">
        <h2 class="display-3 font-weight-bold">Selamat Datang, {{Auth::getUser()->nama}}</h2>
        <br>
        <h3 class="font-weight-bold">Di Sistem Informasi Pelayanan Online</h3>
        <h3 class="font-weight-bold">Kemenag Kabupaten Jombang</h3>
      </div> --}}
      <div class="col-xl-12 col-md-12 col-sm-12 col-12">
        <div class="card border-left-warning shadow h-100">
          <!-- <div class="card-header text-center text-light">
            Grafik Pengajuan Tahun <?=date('Y')?>
          </div> -->
          <div class="card-body">
            <div id="resultChart"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
  resultChartSelesai();

    function resultChartSelesai() {
      $.ajax({
          type: 'POST',
          url: '{{ route('chartBulanan') }}',
          async : true,
          dataType : 'json',
          data: {},
          success: function(vals) { 

          var resurtGrafik = [];
          for (var i = 0; i < vals.dtGrafik.length; i++) {
            resurtGrafik[i] = { name: vals.dtGrafik[i].nama, data: vals.dtGrafik[i].jumlah };
          }
          var judul = vals.title;
          var subJudul = vals.subTitle;
          Highcharts.chart('resultChart', {
            chart: {
                type: 'column'
            },
            title: { text: judul },
            subtitle: { text: subJudul },
            xAxis: {
                categories: [
                    'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                ],
                crosshair: true
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Jumlah Pengajuan'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Kali</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: resurtGrafik
          });
        }
      });
    }
</script>
@stop
