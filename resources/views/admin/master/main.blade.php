<!DOCTYPE html>

<html lang="en">

<head>

  <title>SIP - Sistem Informasi Pelayanan Online</title>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="{{asset('images/logo.png')}}" sizes=""/>

  <meta name="csrf-token" content="{{ csrf_token() }}">

  {{-- <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}"> --}}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

  <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">

  <link rel="stylesheet" href="{{asset('css/animate.css')}}">

  <style>



    .panel-body-card {

      position: relative;

      height: 110px;      

    }



    .bgBlueCard{ background: #2196F3 !important; }

    .bgLightBlueCard{ background: #03A9F4 !important; }

    .bgRedCard{ background: #F44336 !important; }

    .bgPinkCard{ background: #E91E63 !important; }

    .bgLimeCard{ background: #CDDC39 !important; }

    .bgOrangeCard{ background: #FF9800 !important; }

    .bgGreenCard{ background: #4CAF50 !important; }

    .bgPurpleCard{ background: #673AB7 !important; }



    .iconCard {

      -webkit-transition: all .3s linear;

      -o-transition: all .3s linear;

      transition: all .3s linear;

      position: absolute;top: -10px;

      right: 10px;z-index: 0;

      font-size: 70px;

      margin-top: 15px;

      color: white;

    }



    .contenCard {

      margin: 0px;

      font-size: 42px;

      font-weight: bold;

      padding-left: 15px;

      color: white; 

      padding-top:10px

    }

    .Prof {

      font-size: 14px;

      font-weight: bold;

      color: white;

      padding-left: 5px;

    }

    .panel-title-card {

      background: #6f6b6b;

      color: white;

      text-align: center;

      padding: 5px 5px;

    }

  /* Set height of the grid so .sidenav can be 100% (adjust as needed) */

  .row.content {height: 550px}



  /* Set gray background color and 100% height */

  .sidenav {

    background-color: #f1f1f1;

    height: 200%;

    font-weight: Bold;

  }

  div ul .nav{

    background: #ccc;

  }



  /* On small screens, set height to 'auto' for the grid */

  @media screen and (max-width: 767px) {

    .row.content {height: auto;}

  }

  </style>

  @yield('css')

</head>

<body>



  {{-- NAVBAR --}}

  <nav class="navbar navbar-inverse visible-xs">

    <div class="container-fluid">

      <div class="navbar-header">

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="color: black !important">

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

        </button>

        <a class="navbar-brand" href="#">SIP - Sistem Informasi Pelayanan Online</a>

      </div>

      

      @if (Auth::getUser()->level == 1)

        @php

          $total = \DB::table('permohonan')->where('status', 'Diproses')->count();

        @endphp 

      @else

        @php

          $total = \DB::table('permohonan')->where('status', 'pending')->count();

        @endphp 

      @endif



      <div class="collapse navbar-collapse" id="myNavbar">

        <ul class="nav navbar-nav">

          <li class="@if($judul == 'Dashboard') active @endif"><a href="{{route('dashboard')}}">Dashboard</a></li>

          <li class="@if($judul == 'Data Pengajuan') active @endif"><a href="{{route('AdminPengajuan')}}">Data Pengajuan @if ($total > 0)<span class="badge badge-danger">{{$total}}</span>@endif</a></li>

          @if (Auth::getUser()->level == 1)

          <li class="@if($judul == 'Data User') active @endif"><a href="{{route('user')}}">Data User</a></li>

          @endif

          <li class="@if($judul == 'Data Pemohon') active @endif"><a href="{{route('pemohon')}}">Data Pemohon</a></li>



          @if (Auth::getUser()->level == 1)

          <li class="@if($judul == 'Data Layanan') active @endif"><a href="{{route('MasterLayanan')}}">Data Layanan</a></li>

          <li class="@if($judul == 'Data Bagian') active @endif"><a href="{{route('Bagian')}}">Data Bagian</a></li>

          <li class="@if($judul == 'Pengaturan') active @endif"><a href="{{route('Pengaturan')}}">Pengaturan</a></li>
          <li class="@if($judul == 'Laporan') active @endif"><a href="{{route('Laporan')}}">Laporan</a></li>

          @endif



          <li class=""><a href="{{'/logout_admin'}}">Logout</a></li>

        </ul>

      </div>

    </div>

  </nav>





  {{-- SIDEBAR --}}

  <div class="container-fluid">

    <div class="row content">

      <div class="col-sm-2 sidenav hidden-xs">

        <h2>

          <img src="{{url('images/logo.png')}}" width="125px;" style="margin-left: 25px">

        </h2>

        

        <ul class="nav nav-pills nav-stacked">

            <li class="@if($judul == 'Dashboard') active @endif"><a href="{{route('dashboard')}}">Dashboard</a></li>

            <li class="@if($judul == 'Data Pengajuan') active @endif"><a href="{{route('AdminPengajuan')}}">Data Pengajuan @if ($total > 0)<span class="badge badge-danger">{{$total}}</span>@endif</a></li>

            

            @if (Auth::getUser()->level == 1)

            <li class="@if($judul == 'Data User') active @endif"><a href="{{route('user')}}">Data User</a></li>

            @endif



            <li class="@if($judul == 'Data Pemohon') active @endif"><a href="{{route('pemohon')}}">Data Pemohon</a></li>



            @if (Auth::getUser()->level == 1)

            <li class="header text-muted"><a href="#" style="font-size: 13px; color: inherit;text-decoration: inherit;&:link,&:hover {color: inherit;text-decoration: inherit;}"> - Data Master - </a></li>

            <li class="@if($judul == 'Data Layanan') active @endif"><a href="{{route('MasterLayanan')}}">Data Layanan</a></li>

            <li class="@if($judul == 'Data Bagian') active @endif"><a href="{{route('Bagian')}}">Data Bagian/ Lembaga</a></li>

            <li class="@if($judul == 'Pengaturan') active @endif"><a href="{{route('Pengaturan')}}">Pengaturan</a></li>
            <li class="@if($judul == 'Laporan') active @endif"><a href="{{route('Laporan')}}">Laporan</a></li>
            @endif



            <li class=""><a href="{{url('/logout_admin')}}">Logout</a></li>

            <li class="bg-info">

              <a href="{{url('/')}}" target="_blank"><i class="fa fa-globe"></i> Lihat Website</a>

            </li>

        </ul><br>

      </div>

      <br>



      <div class="col-sm-10">

        @yield('content')

      </div>

    </div>

  </div>





  <script src="{{asset('js/jquery.js')}}"></script>

  <script src="{{asset('js/bootstrap.js')}}"></script>

  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  <script src="{{asset('js/jquery.validate.js')}}"></script>

  <script src="{{asset('js/jquery.validate.min.js')}}"></script>

  <script src="{{asset('js/animate.js')}}"></script>

  <script type="text/javascript">

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });



  @if(Session::has('message'))

  swal('{{Session::get('title')}}','{{Session::get('message')}}','{{Session::get('type')}}');

  @endif

  

  </script>

  <script src="{{url('js/datagrid.js')}}"></script>

  

  @yield('js')

</body>

</html>

