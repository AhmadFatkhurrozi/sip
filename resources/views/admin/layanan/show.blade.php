<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-info iconLabel m-r-5"></i> Detail 
    </h4>
    <hr class="m-t-0">

    <form class="form-save">
        <div class="box-body">
            <div class="col-md-12 float-right" style="margin-bottom: 10px;">
                <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Pelayanan<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama_pelayanan }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Bagian/ Lembaga<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama_bagian }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Persyaratan<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->syarat }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
        </form>
    </div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };


</script>
