<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        @if (!empty($data))
            <i class="fa fa-plus-square iconLabel m-r-15"></i> Ubah Data
        @else
            <i class="fa fa-plus-square iconLabel m-r-15"></i> Tambah Data
        @endif
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="box-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="hidden" name="id" value="@if(!empty($data)){{$data->id}}@endif">
                        <input type="text" name="nama" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" value="@if(!empty($data)){{$data->nama}}@endif" placeholder="masukkan nama lengkap">
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="username" id='username' value="@if(!empty($data)){{$data->username}}@endif" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan username">
                        <div class='iconStatus text-green' id='icon_ussername'></div>
                        <p class='messageError errorUsername'></p>
                        <input type='hidden' name='statusUsername' value='Exist' id='statusUsername' class='form-control'>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="email" id='email' required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" value="@if(!empty($data)){{$data->email}}@endif" placeholder="masukkan email">
                        <div class='iconStatus text-green' id='icon_email'></div>
                        <p class='messageError errorMail'></p>
                        <input type='hidden' name='statusMail' value='Exist' id='statusMail' class='form-control'>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Password<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="password" name="password" class="form-control" autocomplete="new-password" placeholder="kosongi jika tidak ingin mengubah password">
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Level User<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                           <label><input type="radio" name="level" value="1" @if(!empty($data) && $data->level == 1) checked="" @endif class="flat-red custom-control-input" id="level1"> <span class="p-l-5">Admin</span></label>
                        </div>
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                           <label><input type="radio" name="level" value="2" @if(!empty($data) && $data->level == 2) checked="" @endif class="flat-red custom-control-input" id="level2"> <span class="p-l-5">Staf</span></label>
                        </div>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Bagian<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                      <select name="bagian_id" class="form-control">
                        @if (empty($data))
                          <option disabled="" selected=""> -- Pilih Bagian --</option>
                        @endif
                            <option value="0">ADMIN</option>
                          @foreach ($bagian as $a)
                          <option value="{{$a->id_bagian}}" @if (!empty($data) && $data->bagian_id == $a->id_bagian) selected="" @endif>{{$a->nama_bagian}}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right" style="margin-right: 10px;">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right" style="margin-right: 10px;"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#panel-add').animateCss('bounceInUp');
    })();

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('#panel-add').animateCss('bounceOutDown');
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });


  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addUser') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
