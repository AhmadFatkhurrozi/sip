<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-info iconLabel m-r-5"></i> Detail 
    </h4>
    <hr class="m-t-0">

    <form class="form-save">
        <div class="box-body">
            <div class="col-md-12 float-right" style="margin-bottom: 10px;">
                <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>NIK</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nik }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->username }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->email }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No. Telp</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->no_telp }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
                
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Kelamin</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->jenis_kelamin }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->alamat }}</span>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
            </div>
        </form>
    </div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

</script>
