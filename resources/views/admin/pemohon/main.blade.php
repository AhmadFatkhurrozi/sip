@extends('admin.master.main')
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-primary main-layer">
            <div class="panel-heading">
                {{$judul}}
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer" style='padding:5px'>
                {{-- <button type="button" class="btn btn-sm btn-primary btn-add">
                    <span class="fa fa-plus"></span> &nbsp Tambah
                </button> --}}
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12 form-inline main-layer" style="text-align: right;padding:5px;">
                <div class="form-group">
                    <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>
                </div>
                <div class="form-group">
                    <input type="text" class="input-sm form-control" placeholder="Search" id="search">
                </div>
            </div>
            <div class='clearfix'></div>
            <div class="col-md-12" style='padding:0px'>
                <div class="table-responsive" style="min-height: 200px;">
                    <table class="table table-striped b-t b-light" id="datagrid"></table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-1 hidden-xs">
                            <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                        </div>
                        <div class="col-sm-6 text-center">
                            <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                        </div>
                        <div class="col-sm-5 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>
                        </div>
                    </div>
                </footer>
            </div>
            <div class='clearfix'></div>
        </div>
    </div>
</div>
<div class="other-page"></div>
<div class="modal-dialog"></div>
@stop

@section('js')
  <script type="text/javascript">
    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('datagridPemohon') !!}",
      primaryField          : 'id_pemohon',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'username', title: 'Username', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'email', title: 'Email', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'nama', title: 'Nama', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'nik', title: 'NIK', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'menu', title: 'Menu', sortable: false, width: 200, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return menu(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

  
    function menu(rowData, rowIndex) {
        var menu =
            '<a href="javascript:void(0);" class="btn btn-xs m-r-3 btn-primary" onclick="detail('+rowIndex+')"><i class="fa fa-eye"></i> Detail</a>';
       
        return menu;
    }

    function detail(rowIndex){
        var rowData = datagrid.getRowData(rowIndex);
        $('.loading').show();
        $('.main-layer').hide();
        $.post("{!! route('showPemohon') !!}",{id:rowData.id_pemohon}).done(function(data){
            if(data.status == 'success'){
                $('.loading').hide();
                $('.other-page').html(data.content).fadeIn();
            } else {
                $('.main-layer').show();
            }
        });
    }
  </script>
@stop
