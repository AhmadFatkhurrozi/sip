<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-info m-r-15 m-l-5"></i> Upload Sertifikat</h5>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form  method="POST" enctype="multipart/form-data" action="{{ route('addSertifikat') }}">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
             <div class="col-md-12">
                <div class="form-group">
                    <label>Sertifikat<span class="text-red">(PDF)</span></label>
                    <input type="hidden" value="{{$id}}" name="id">
                        <input type="file" class="upload" name="berkas" accept="application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12">
                </div>
                <div class='clearfix p-b-5'></div>
             </div> 
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-rounded btn-primary btn-submit pull-right">UPLOAD</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#detail-dialog').modal('show');
    })();
    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    })



</script>
