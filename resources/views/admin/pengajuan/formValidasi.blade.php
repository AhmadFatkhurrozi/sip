<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-info m-r-15 m-l-5"></i> Konfirmasi Pengajuan</h5>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form class="addPersentase">
        <div class="modal-body">
          <div class="row">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{date('d-m-Y', strtotime($data->tanggal))}}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nomor</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nomor_bukti }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Untuk Permohonan</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama_pelayanan }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Berkas Permohonan</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: <a href="{!! url('/'.$data->berkas) !!}"><i class="fa fa-file"></i> Formulir</a></span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>NIK</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nik }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 

                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->username }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 

                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->email }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 
           
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No. Telp</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->no_telp }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 
                
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Kelamin</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->jenis_kelamin }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat</label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->alamat }}</span>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div> 
            </div>

           

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group row m-b-0">
                <div class="col-md-12">
                
                  <input type="hidden" name="id" id="id" value="{{ $id }}" class="form-control form-control-sm">
                  <label>Catatan</label>
                 <textarea name="catatan" id="catatan" class="form-control" rows="4" placeholder="catatan"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          @if (Auth::getUser()->level == 2)
            <a href="javascript:void(0)" onclick="approve('Ditolak Staf')" class="btn btn-rounded btn-danger pull-left">TOLAK</a>
            <a href="javascript:void(0)" onclick="approve('Diproses')" class="btn btn-rounded btn-primary btn-submit pull-right">TERIMA</a>
          @else
            <a href="javascript:void(0)" onclick="approve('Ditolak Admin')" class="btn btn-rounded btn-danger pull-left">TOLAK</a>
            <a href="javascript:void(0)" onclick="approve('Diterima')" class="btn btn-rounded btn-primary btn-submit pull-right">TERIMA</a>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#detail-dialog').modal('show');
    })();
    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    })


  function approve(status) {
    var id = $('#id').val();
    var catatan = $('#catatan').val();
    swal(
    {
      title: "Konfirmasi Permohonan ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Saya yakin!",
      cancelButtonText: "Batal!",
      closeOnConfirm: false
    },
      function(){
          $.post("{!! route('approve') !!}", {id:id, catatan:catatan, status:status}).done(function(data){
              if(data.status == 'success'){
                  $('#detail-dialog').modal('hide');
                  swal({
                    title : "Berhasil",
                    text : "Konfirmasi Berhasil !",
                    type : "success",
                    timer: 2000,
                    showConfirmButton: false
                  });
                  location.reload();

              }else if(data.status == 'error'){
                  location.reload();
              swal("Maaf !!!", "Konfirmasi Gagal !!", "warning");
              }
          });
      }
  );

  }

</script>
