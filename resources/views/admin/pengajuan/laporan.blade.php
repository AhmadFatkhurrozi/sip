@extends('admin.master.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://app.cahayakhitan.com/css/bootstrap-datetimepicker.min.css">
    
@stop

@section('content')

<div class="row">

    <div class="col-lg-12 col-md-12">

        <div class="panel panel-primary main-layer">

            <div class="panel-heading">

                {{$judul}}

            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer" style='padding:5px'>

                {{-- <button type="button" class="btn btn-sm btn-primary btn-add">

                    <span class="fa fa-plus"></span> &nbsp Tambah

                </button> --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                  <input type="text" class="input-sm form-control bulan" data-date-format="yyyy-mm" placeholder="periode">
              </div>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-danger doPrint" title="Cetak"><i class="fa fa-print"></i>&nbsp; Cetak </button>
                    </div>
                </div>

            </div>

            <div class="col-md-8 col-sm-8 col-xs-12 form-inline main-layer" style="text-align: right;padding:5px;">

                <div class="form-group" style="display: none;">

                    <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>

                </div>

                <div class="form-group" style="display: none;">

                    <input type="text" class="input-sm form-control" placeholder="Search" id="search">

                </div>

            </div>

            <div class='clearfix'></div>

            <div class="col-md-12" style='padding:0px'>

                <div class="table-responsive" style="min-height: 200px;">

                    <table class="table table-striped b-t b-light" id="datagrid"></table>

                </div>

                <footer class="panel-footer">

                    <div class="row">

                        <div class="col-sm-1 hidden-xs">

                            <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>

                        </div>

                        <div class="col-sm-6 text-center">

                            <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>

                        </div>

                        <div class="col-sm-5 text-right text-center-xs">

                            <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>

                        </div>

                    </div>

                </footer>

            </div>

            <div class='clearfix'></div>

        </div>

    </div>

</div>

<div class="other-page"></div>

<div class="modal-dialog"></div>

@stop



@section('js')
    <script type="text/javascript" src="https://app.cahayakhitan.com/js/bootstrap-datetimepicker.min.js"></script>
  
  <script type="text/javascript">

    var datagrid = $("#datagrid").datagrid({

        url                     : "{!! route('AdminPengajuanDatagrid') !!}",

        primaryField            : 'id_permohonan', 

        rowNumber               : true, 

        rowCheck                : false, 

        searchInputElement      : '#search', 

        searchFieldElement      : '#search-option', 

        pagingElement           : '#paging', 

        optionPagingElement     : '#option',

        pageInfoElement         : '#info',
         queryParams             : {
                  'periode'    : $('.bulan').val(),
              },

        columns                 : [

            {field: 'tanggal', title: 'Tanggal', editable: false, sortable: true, width: 100, align: 'center', search: true},

            {field: 'nomor_bukti', title: 'Nomor Bukti', sortable: true, width: 200, align: 'center', search: true},

            {field: 'nama', title: 'Nama Pemohon', editable: false, sortable: true, width: 100, align: 'center', search: true},
            {field: 'nama_bagian', title: 'Nama Bagian', editable: false, sortable: true, width: 100, align: 'center', search: true},

            {field: 'nama_pelayanan', title: 'Nama Pelayanan', editable: false, sortable: true, width: 200, align: 'left', search: true},
            {field: 'status', title: 'Status', editable: false, sortable: true, width: 100, align: 'center', search: true},

        ]

    });



    $(document).ready(function() {

        datagrid.run();

    });

    $(".bulan").on("change", function(){
            datagrid.queryParams({
                'periode'    : $('.bulan').val()
            });
            datagrid.reload();
        });

    $('.bulan').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 3,
        forceParse: 0,
      });

       $('.doPrint').click(function() {
        var bulan = $('.bulan').val();
        window.open("{{ route('cetakLaporan') }}?bulan="+bulan, '_blank');
      });

  </script>

@stop

