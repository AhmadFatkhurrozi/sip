<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        @if (!empty($data))
            <i class="fa fa-plus-square iconLabel m-r-15"></i> Ubah Data
        @else
            <i class="fa fa-plus-square iconLabel m-r-15"></i> Tambah Data
        @endif
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Pelayanan<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="hidden" name="id" value="@if(!empty($data)){{$data->id_pelayanan}}@endif">
                        <input type="text" name="nama_pelayanan" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" value="@if(!empty($data)){{$data->nama_pelayanan}}@endif" placeholder="masukkan nama pelayanan">
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>
           
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Bagian<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                      <select name="bagian_id" class="form-control">
                        @if (empty($data))
                          <option disabled="" selected=""> -- Pilih Bagian --</option>
                        @endif
                          @foreach ($bagian as $a)
                          <option value="{{$a->id_bagian}}" @if (!empty($data) && $data->bagian_id == $a->id_bagian) selected="" @endif>{{$a->nama_bagian}}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Syarat<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                       <textarea name="syarat" class="form-control" rows="5">@if (!empty($data)){{$data->syarat}}@endif</textarea>
                    </div>
                </div>
                <div class='clearfix' style="padding-bottom: 10px;"></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Formulir<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="file" class="upload" name="berkas" accept=
"application/msword, .docx, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right" style="margin-right: 10px;">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right" style="margin-right: 10px;"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#panel-add').animateCss('bounceInUp');
    })();

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('#panel-add').animateCss('bounceOutDown');
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });


  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addMasterLayanan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
