<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Nomor Bukti</title>
    <style media="screen">
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
      }
      .detail, .detail tbody td,.detail tbody tr, .detail thead tr,.detail thead th{
        /*border :0.5px solid black;*/
        border-spacing: 0.5px;
        /*text-align: center;*/
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 12px;
      }
      .detail thead tr {
        background-color: royalblue;
        color: #fff;
      }
      .detail tbody td{
        padding : 5px;
        /*text-align: center;*/
      }
      .detail tbody tr:nth-child(odd){
        background: #ebebeb;
      }
      .detail tbody tr:nth-child(even){
        background: #d7d7d7;
      }
      @page { margin: 30px; }
      body{margin:30px;}
      .title, .dept {
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      .dept {
        font-size: 13px !important;
      }
    </style>
  </head>
  <body>
    <div style="margin: 30px; text-align: center;">
      <div class="title">Laporan SI - Pelayanan Online</div>
      <div class="title">{{$judul}}</div>
    </div>
    <table style="margin-top: 30px; font-size: 12px;" border="1">
        <thead>
         <tr>
            <th style="text-align: center !important;">No.</th>
            <th style="text-align: center !important;">Tanggal</th>
            <th style="text-align: center !important;">Nomor Bukti</th>
            <th>Nama Pemohon</th>
            <th>Bagian</th>
            <th>Nama Layanan</th>
            <th style="text-align: center !important;">Status</th>
           </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>

          @if(count($data) > 0)
            @foreach ($data as $a)
            <tr>
              <td align="center">{{$no++}}</td>
              <td align="center">{{date('d-m-Y', strtotime($a->tanggal))}}</td>
              <td align="center">{{$a->nomor_bukti}}</td>
              <td>{{$a->nama}}</td>
              <td>{{$a->nama_bagian}}</td>
              <td>{{$a->nama_pelayanan}}</td>
              <td align="center">{{$a->status}}</td>
            </tr>
            @endforeach
          @else
          <tr>
            <td colspan="7" align="center"><i>data tidak ada</i></td>
          </tr>
          @endif

        </tbody>
    </table> 
  </body>
</html>
