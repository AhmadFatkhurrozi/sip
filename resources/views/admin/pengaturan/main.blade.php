@extends('admin.master.main')
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <form method='post' action="{{ route('changeIdentity') }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            <div class="panel panel-primary main-layer">
                <div class="panel-heading">
                    {{$judul}}
                </div>
                <div class="panel-body">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default main-layer">
                            <div class="panel-heading"><h4>Alur</h4></div>
                            <div class="panel-body">
                                <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12' style='padding:0px'>
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="crop-edit">
                                                <center>
                                                    @if(!empty($identity->alur))
                                                        <img id="preview-photo" src="{!! url('uploads/'.$identity->alur) !!}" class="img-polaroid text-center" width="400px">
                                                    @else
                                                        <img id="preview-photo" src="{!! url('images/default.png') !!}" class="img-polaroid text-center" width="400px">
                                                    @endif
                                                </center>
                                            </div>
                                            <div class='clearfix' style='padding-bottom:5px'></div>
                                            <input type="file" class="upload" onchange="loadFilePhoto(event)" name="alur" accept="image/jpeg" class="form-control customInput col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                                <div class='clearfix' style='padding-bottom:25px'></div>
                            </div>
                        </div> <!-- form-panel -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default main-layer">
                            <div class="panel-heading"><h4>Header Image</h4></div>
                            <div class="panel-body">
                                <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12' style='padding:0px'>
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="crop-edit">
                                                <center>
                                                    @if(!empty($identity->header))
                                                        <img id="preview-photo2" src="{!! url('uploads/'.$identity->header) !!}" class="img-polaroid text-center" width="400px">
                                                    @else
                                                        <img id="preview-photo2" src="{!! url('images/default.png') !!}" class="img-polaroid text-center" width="400px">
                                                    @endif
                                                </center>
                                            </div>
                                            <div class='clearfix' style='padding-bottom:5px'></div>
                                            <input type="file" class="upload" onchange="loadFilePhoto2(event)" name="header" accept="image/jpeg" class="form-control customInput col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                                <div class='clearfix' style='padding-bottom:25px'></div>
                            </div>
                        </div> <!-- form-panel -->
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="other-page"></div>
<div class="modal-dialog"></div>
@stop

@section('js')
<script type="text/javascript">
    function loadFilePhoto(event) {
        var image = URL.createObjectURL(event.target.files[0]);
            $('#preview-photo').fadeOut(function(){
                $(this).attr('src', image).fadeIn().css({
                    '-webkit-animation' : 'showSlowlyElement 700ms',
                    'animation'         : 'showSlowlyElement 700ms'
                });
            });
    };

    function loadFilePhoto2(event) {
        var image = URL.createObjectURL(event.target.files[0]);
        $('#preview-photo2').fadeOut(function(){
            $(this).attr('src', image).fadeIn().css({
                '-webkit-animation' : 'showSlowlyElement 700ms',
                'animation'         : 'showSlowlyElement 700ms'
            });
        });
    };
</script>
@stop
