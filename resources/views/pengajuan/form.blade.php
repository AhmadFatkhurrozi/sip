@extends('layout.main')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ route('addPengajuan') }}">
  {{ csrf_field() }}
  <div class="row">
   
    <div class="col-lg-6 col-md-6 offset-md-3 col-sm-12">
      <h1 class="my-5">{{$judul}}</h1>
      <div class="form-group">
        <label>Layanan</label>
        <select name="pelayanan_id" class="form-control">
            <option disabled="" selected=""> -- Pilih Layanan --</option>
            @foreach ($layanan as $a)
            <option value="{{$a->id_pelayanan}}">{{$a->nama_pelayanan}}</option>
            @endforeach
        </select>
      </div>
   
        <div class="form-group">
            <label>Formulir<span class="text-red">(PDF)</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="file" class="upload" name="berkas" accept="application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12">
            </div>
        </div>
        <div class='clearfix p-b-5'></div>
        
        <div class="form-group">
            <label>Lampiran<span class="text-red">(Jika ada, dalam bentuk PDF)</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="file" class="upload" name="lampiran" accept="application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12">
            </div>
        </div>
        <div class='clearfix p-b-5'></div>

        <button type="submit" class="btn btn-success btn-xs pull-right">Tambah</button>
    </div>

  </div>
</form>
@endsection

@section('js')

@endsection