<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Nomor Bukti</title>
    <style media="screen">
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 80%;
      }
      .detail, .detail tbody td,.detail tbody tr, .detail thead tr,.detail thead th{
        /*border :0.5px solid black;*/
        border-spacing: 0.5px;
        /*text-align: center;*/
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 12px;
      }
      .detail thead tr {
        background-color: royalblue;
        color: #fff;
      }
      .detail tbody td{
        padding : 5px;
        /*text-align: center;*/
      }
      .detail tbody tr:nth-child(odd){
        background: #ebebeb;
      }
      .detail tbody tr:nth-child(even){
        background: #d7d7d7;
      }
      @page { margin: 30px; }
      body{margin:30px;}
      .title, .dept {
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      .dept {
        font-size: 13px !important;
      }
    </style>
  </head>
  <body>
    <div style="margin: 30px; text-align: center;">
      <div class="title">PERMOHONAN</div>
      <div class="title">{{$judul}}</div>
    </div>
    <table style="margin-top: 30px;">
        <tr>
          <td>NIK</td>
          <td> : {{$data->nik}}</td>
        </tr>
        <tr>
          <td>Nama</td>
          <td> : {{$data->nama}}</td>
        </tr>
        <tr>
          <td>Pengajuan</td>
          <td> : {{$data->nama_pelayanan}}</td>
        </tr>
    </table> 
  </body>
</html>
