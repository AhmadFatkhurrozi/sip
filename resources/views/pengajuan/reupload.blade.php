<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-info m-r-15 m-l-5"></i> Upload Ulang Berkas Pengajuan</h5>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="POST" enctype="multipart/form-data" action="{{ route('saveReupload') }}">
      {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$data->id_permohonan}}">
        <div class="modal-body">
          <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{date('d-m-Y', strtotime($data->tanggal))}}</span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nomor</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nomor_bukti }}</span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div>
           
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Untuk Permohonan</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: {{ $data->nama_pelayanan }}</span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Berkas Permohonan</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span>: <a href="{!! url('/'.$data->berkas) !!}"><i class="fa fa-file"></i> Formulir</a></span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div> 

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Upload Berkas Baru</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span><input type="file" class="upload" name="berkasBaru" accept="application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12"></span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div> 
                
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Upload Lampiran Baru</label>
                    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <span><input type="file" class="upload" name="lampiran" accept="application/pdf" class="form-control customInput input-sm col-md-7 col-xs-12"></span>
                    </span>
                </div>
                <div class='clearfix p-b-5'></div> 
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#detail-dialog').modal('show');
    })();
    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    })


  function approve(status) {
    var id = $('#id').val();
    var catatan = $('#catatan').val();
    swal(
    {
      title: "Konfirmasi Permohonan ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Saya yakin!",
      cancelButtonText: "Batal!",
      closeOnConfirm: false
    },
      function(){
          $.post("{!! route('approve') !!}", {id:id, catatan:catatan, status:status}).done(function(data){
              if(data.status == 'success'){
                  $('#detail-dialog').modal('hide');
                  swal({
                    title : "Berhasil",
                    text : "Konfirmasi Berhasil !",
                    type : "success",
                    timer: 2000,
                    showConfirmButton: false
                  });
                  location.reload();

              }else if(data.status == 'error'){
                  location.reload();
              swal("Maaf !!!", "Konfirmasi Gagal !!", "warning");
              }
          });
      }
  );

  }

</script>
