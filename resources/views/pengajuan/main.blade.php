@extends('layout.main')

@section('content')



<div class="col-lg-12 col-md-12">

    <h1>{{$judul}}</h1>

    <div class="pull-right"> <a href="{{ route('formPengajuan') }}" class="btn btn-primary btn-sm m-2">Tambah Pengajuan</a> </div>

    <table class="table table-striped table-bordered">

        <thead>

            <tr>

                <th>No</th>

                <th width="120px">Tanggal</th>

                <th>Nomor Bukti</th>

                <th>Pengajuan</th>

                <th width="150px">Berkas</th>

                <th>Status</th>

                <th width="220px">Catatan</th>

            </tr>

        </thead>

        <tbody>

            @php

                $no = 1;

            @endphp



            @if (count($pengajuan) > 0)

                @foreach ($pengajuan as $a)

                    <?php 

                        if($a->status == "pending"){

                            $stts = "Menunggu Konfirmasi Staf";

                            $bdg = "badge badge-info";

                        }else if($a->status == "Diproses"){

                            $stts = "Menunggu Konfirmasi Admin";

                            $bdg = "badge badge-info";

                        }else if($a->status == "Diterima"){

                            if($a->tanggal_diterima!=''){

                                $stts = $a->status." pada tanggal ".date('d-m-Y', strtotime($a->tanggal_diterima));

                            }else{

                                $stts = $a->status;

                            }

                            

                            $bdg = "";

                        }else{

                            $stts = $a->status;

                            $bdg = "badge badge-danger";

                        }

                    ?>

                <tr>

                    <td class="bg-light text-center">{{$no++}}</td>

                    <td class="bg-light text-center">{{date('d-m-Y', strtotime($a->tanggal))}}</td>

                    <td class="bg-light text-center">{{$a->nomor_bukti}}</td>

                    <td class="bg-light">{{$a->nama_pelayanan}}</td>

                    <td class="bg-light text-center">

                        <a href="{!! url('/'.$a->berkas) !!}"><i class="fa fa-file"></i> Formulir</a><br>

                        @if(!empty($a->lampiran))

                            <a href="{!! url('/'.$a->lampiran) !!}"><i class="fa fa-file"></i> Lampiran</a>

                        @endif

                    </td>

                    <td class="bg-light text-center"><span class='{{$bdg}}'>{{$stts}}</span></td>

                    <td class="bg-light text-center">@if ($a->catatan != '') {{$a->catatan}}, <br> @endif

                        <span>

                            @if ($stts == "Ditolak Staf" || $stts == "Ditolak Admin")

                            <a href="javascript:void(0)" onclick="reupload('{{$a->id_permohonan}}')" class="font-weight-bold">Upload Ulang FIle</a>

                            @endif

                            @if ($stts == "Menunggu Konfirmasi Staf")

                            <a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="batalkan({{$a->id_permohonan}})" class="font-weight-bold"><i class="fa fa-info"></i> Batalkan</a>

                            @endif

                            @if ($a->status == "Diterima")
                                <small><a href="javascript:void(0)" onclick="cetakNoBukti({{$a->id_permohonan}})">Cetak Nomor Bukti</a> untuk pengambilan sertifikat</small>
                            @endif
                        </span><br>


                        @if ($a->sertifikat != '')
                            <small>atau download
                            <a href="{!! url('/'.$a->sertifikat) !!}">E-Sertifikat</a>
                            </small>
                        @endif

                    </td>

                </tr>

                @endforeach

            @else

            <tr>

                <td colspan="5" class="text-center bg-light"><i>-- tidak ada pengajuan --</i></td>

            </tr>

            @endif

        </tbody>

    </table>

</div>

    

<div class="other-page"></div>

<div class="modal-dialog"></div>

@endsection



@section('js')

    <script type="text/javascript">

        function reupload(id) {

            $.post("{!! route('reupload') !!}", {id:id}).done(function(data){

                if(data.status == 'success'){

                    $('.modal-dialog').html(data.content);

                }

            });

        }



        function batalkan(id) {

            swal(

                {

                    title: "Apa anda yakin membatalkan pengajuan ini ?",

                    text: "",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonColor: "#DD6B55",

                    confirmButtonText: "Saya yakin!",

                    cancelButtonText: "Batal!",

                    closeOnConfirm: true

                },

                function(){

                    $.post("{!! route('batalPengajuan') !!}", {id:id}).done(function(data){

                        if(data.status == 'success'){

                            location.reload();

                            $('.main-layer').show();

                            $('.loading').hide();

                            swal(data.title, data.message, data.type);

                        }else if(data.status == 'error'){

                            location.reload();

                            swal(data.title, data.message, data.type);

                        }

                    });

                }

            );

        }

        function cetakNoBukti(id) {
            window.open("{{ route('cetakNoBukti') }}?id="+id, '_blank');
        }
    </script>

@stop



