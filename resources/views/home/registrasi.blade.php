@extends('layout.main')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ route('regis_store') }}">
  {{ csrf_field() }}
  <div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12">
      <h1 class="mt-3">Daftar</h1>
	  </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" required="" autocomplete="false" class="form-control" placeholder="masukkan username (exp: budiman123)">
      </div>

      <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" required="" autocomplete="new-password" class="form-control password" placeholder="masukkan password baru">
      </div>

      <div class="form-group">
        <label>Konfirmasi Password</label>
        <input type="password" name="confirm_password" onkeyup="samePass()" required="" class="form-control confirm_password" placeholder="konfirmasi password">
      </div>

      <div class="form-group">
        <label>NIK</label>
        <input type="text" name="nik" required="" class="form-control" placeholder="masukkan NIK (exp: 3517061709990001)">
      </div>

      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="nama" required="" class="form-control" placeholder="masukkan nama lengkap (exp: budiman supriyadi)">
      </div>
      
      <div class="form-group">
        <label>Status Pekerjaan : </label><br>
        {{-- <label class="radio-inline mr-3">
            <input type="radio" name="pekerjaan" value="PNS"><b>PNS</b>
          </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" value="Dosen"><b>Dosen</b>
        </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" value="Mahasiswa"><b>Mahasiswa</b>
        </label>
        <label class="radio-inline mr-3">
          <input type="radio" name="pekerjaan" value="Umum"><b>Umum</b>
        </label> --}}
        <input type="text" name="pekerjaan" class="form-control" placeholder="masukkan pekerjaan anda (exp: mahasiswa)">
      </div>
    </div>


    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
          <label>Jenis Kelamin</label>
          <select class="form-control" name="gender">
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>
        </div>
    
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" required="" class="form-control" placeholder="masukkan email (exp: budiman@gmail.com)">
          <small class="form-text text-muted">Gunakan Email aktif.</small>
        </div>
      
        <div class="form-group">
          <label>Nomor Telepon</label>
          <input type="text" name="no_telp" class="form-control" placeholder="masukkan nomor telp/ hp (exp: 085612341234)">
        </div>
      
        <div class="form-group">
          <label>Alamat</label>
          <textarea class="form-control" name="alamat" placeholder="masukkan alamat lengkap anda"></textarea>
        </div>

        <button type="submit" class="btn btn-success btn-xs btn-block">Daftar</button>
    </div>

  </div>
</form>
@endsection

@section('js')
<script type="text/javascript">

  function samePass() {
  	var password = $('.password').val();
  	var confirm_password = $('.confirm_password').val();

  	if (password == confirm_password) {
  		$('.statusPass').html('');
  	}else{
  		$('.statusPass').html('password tidak sesuai');
  	}
  }

</script>
@endsection