@extends('layout.main')
@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 offset-md-3">
            <h1 class="my-3">Login</h1>
            <form method="post" action="{{route('doLogin')}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label><b>Username</b></label>
                <input type="text" name="username" autocomplete="off" class="form-control" required>
            </div>
            <hr class="clearfix mt-2"></hr>

            <div class="form-group">
                <label><b>Password</b></label>
                <input type="password" name="password" autocomplete="off" class="form-control" required>
            </div>
            <hr class="clearfix mt-2"></hr>

            <div>
                <button type="submit" class="btn btn-outline-success btn-sm btn-block my-3">Login</button>
            </div>
            
            <span class="psw">Belum punya akun ? <a href="{{ url('registrasi/') }}" style="font-weight:bold; color: green;"> Daftar Sekarang !</a></span>

            </form>
        </div>   
    </div>
</div>
@endsection
