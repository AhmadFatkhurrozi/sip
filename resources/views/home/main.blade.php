@extends('layout.main')
@section('content')
<div class="col-lg-12 col-md-12">
    <h1>Selamat Datang Di Sistem Pelayanan Online Kemenag Jombang</h1>

    <h2 class="py-3">Daftar Pelayanan</h2>
    @php
        $data = \DB::table('master_pelayanan')->get();
    @endphp

    <ol>
        @foreach ($data as $a)
        <li>{{$a->nama_pelayanan}}</li>
        @endforeach
    </ol>
    
    <div class="text-center">
        <a href="{{ route('frontPelayanan') }}" class="btn btn-success btn-sm">Lihat Persyaratan</a>
    </div>
    <br>
    <hr>
    <br>
    <h3 class="text-center">ALUR PENGAJUAN</h3>
    <br>
    <?php $alur = \DB::table('pengaturan')->where('id', 1)->first()->alur; ?>
    @if (!empty($alur))
    <div class="text-center">
        <img src="{{ url('/uploads/'.$alur) }}" class="img-fluid text-center" style="width: 60%;">
    </div>
    @endif
</div>
@endsection
