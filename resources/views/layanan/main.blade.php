@extends('layout.main')
@section('content')

<div class="col-lg-12 col-md-12">
    <h1>{{$judul}}</h1>

    <div class="row">
        <div class="col-md-12">
           {{--  @foreach ($layanan as $a)
                <div id="accordion">
                    <div class="card">
                        <div class="card-header bg-primary" id="heading{{$a->id_pelayanan}}">
                          <h5 class="mb-0">
                            <button class="btn btn-link font-weight-bold" style="color: white !important;" data-toggle="collapse" data-target="#collapse{{$a->id_pelayanan}}" aria-expanded="true" aria-controls="collapse{{$a->id_pelayanan}}">
                              {{$a->nama_pelayanan}} - <span class="text-dark">{{$a->nama_bagian}}</span>
                            </button>
                          </h5>
                        </div>

                        <div id="collapse{{$a->id_pelayanan}}" class="collapse show" aria-labelledby="heading{{$a->id_pelayanan}}" data-parent="#accordion">
                            <div class="card-body">
                                {{$a->syarat}} <a href="{{$a->formulir}}" class="btn btn-success btn-sm mx-2">Download Berkas</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach --}}
            <table class="table table-striped table-responsive table-hover">
                <thead class="bg-primary text-light">
                    <tr>
                        <th align="center">No.</th>
                        <th>Nama Layanan</th>
                        <th>Bagian</th>
                        <th>Persyaratan</th>
                        <th width="200px">Formulir</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    @foreach ($layanan as $a)
                    <tr>
                        <td align="center">{{$no++}}</td>
                        <td>{{$a->nama_pelayanan}}</td>
                        <td>{{$a->nama_bagian}}</td>
                        <td>{{$a->syarat}}</td>
                        <td><a href="{{$a->formulir}}" class="btn btn-success btn-sm mx-2">Download Berkas</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
   
</div>
@endsection
