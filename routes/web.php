<?php



Route::get('/sendmail', 'NotifController@sendmail');

Route::get('/logout', 'Site\LoginController@logout');



Route::get('/', 'Site\DashboardController@main');



Route::group(['prefix'=>'login'],function(){

  Route::get('/','Site\LoginController@main')->name('login');

  Route::post('/doLogin','Site\LoginController@doLogin')->name('doLogin');

});





Route::group(['prefix'=>'registrasi'],function(){

  Route::get('/','Site\LoginController@registrasi');

  Route::post('/store','Site\LoginController@store')->name('regis_store');

});



// user athor route



Route::group(['middleware'=>'userall'],function(){

  Route::group(['prefix'=>'pengajuan'],function(){

    Route::get('/','Site\PengajuanController@main')->name('Pengajuan');

    Route::get('/form','Site\PengajuanController@form')->name('formPengajuan');

    Route::post('/add','Site\PengajuanController@add')->name('addPengajuan');

    Route::post('/reupload','Site\PengajuanController@reupload')->name('reupload');

    Route::post('/saveReupload','Site\PengajuanController@saveReupload')->name('saveReupload');

    Route::post('/batalPengajuan',  'Site\PengajuanController@batalPengajuan')->name('batalPengajuan');
    Route::get('/cetakNoBukti',  'Site\PengajuanController@cetakNoBukti')->name('cetakNoBukti');

  });



  Route::group(array('prefix'=>'profile'), function(){

      Route::get('/', 'ProfileController@index')->name('profile');

      Route::post('/update', 'ProfileController@update')->name('updateProfil');

  });

  

});



Route::get('/log_admin','Site\LoginController@log_admin')->name('log_admin');

Route::post('/dolog_admin','Site\LoginController@dolog_admin')->name('dolog_admin');

Route::get('/logout_admin', 'Site\LoginController@logout_admin');



Route::group(['prefix'=>'pelayanan'],function(){

  Route::get('/','Site\PelayananController@main')->name('frontPelayanan');

});



Route::group(['middleware'=>'admin'],function(){

  Route::group(['prefix'=>'admin'],function(){

    Route::get('/','Admin\DashboardController@main')->name('dashboard');

    

    Route::group(['prefix'=>'user'],function(){

      Route::get('/','Admin\UserController@main')->name('user');

      Route::post('/datagrid','Admin\UserController@datagrid')->name('userDatagrid');

      Route::post('/form-add',  'Admin\UserController@form')->name('formUser');

      Route::post('/add',  'Admin\UserController@add')->name('addUser');

      Route::post('/show',  'Admin\UserController@show')->name('showUser');

      Route::post('/delete',  'Admin\UserController@delete')->name('deleteUser');

    });



    Route::group(['prefix'=>'pemohon'],function(){

      Route::get('/','Admin\PemohonController@main')->name('pemohon');

      Route::post('/datagrid','Admin\PemohonController@datagrid')->name('datagridPemohon');

      Route::post('/show',  'Admin\PemohonController@show')->name('showPemohon');

    });



    Route::group(['prefix'=>'pengajuan'],function(){

        Route::get('/','Admin\PengajuanController@main')->name('AdminPengajuan');

        Route::post('/datagrid','Admin\PengajuanController@datagrid')->name('AdminPengajuanDatagrid');

        Route::post('/formValidasi',  'Admin\PengajuanController@formValidasi')->name('formValidasi');

        Route::post('/approve',  'Admin\PengajuanController@approve')->name('approve');

        Route::post('/show',  'Admin\PengajuanController@show')->name('showPengajuan');

        Route::post('/uploadSertifikat',  'Admin\PengajuanController@uploadSertifikat')->name('uploadSertifikat');

        Route::post('/addSertifikat',  'Admin\PengajuanController@addSertifikat')->name('addSertifikat');

        Route::post('/chartBulanan',  'Admin\PengajuanController@chartBulanan')->name('chartBulanan');

        Route::get('/Laporan',  'Admin\PengajuanController@Laporan')->name('Laporan');
        Route::get('/cetak-laporan',  'Admin\PengajuanController@cetakLaporan')->name('cetakLaporan');
        // Route::post('/delete',  'Admin\BagianController@delete')->name('deleteBagian');

      });

    

    Route::group(['prefix'=>'master'],function(){

      Route::group(['prefix'=>'layanan'],function(){

        Route::get('/','Admin\LayananController@main')->name('MasterLayanan');

        Route::post('/datagrid','Admin\LayananController@datagrid')->name('MasterLayananDatagrid');

        Route::post('/form-add',  'Admin\LayananController@form')->name('formMasterLayanan');

        Route::post('/add',  'Admin\LayananController@add')->name('addMasterLayanan');

        Route::post('/show',  'Admin\LayananController@show')->name('showMasterLayanan');

        Route::post('/delete',  'Admin\LayananController@delete')->name('deleteMasterLayanan');

      });



      Route::group(['prefix'=>'bagian'],function(){

        Route::get('/','Admin\BagianController@main')->name('Bagian');

        Route::post('/datagrid','Admin\BagianController@datagrid')->name('BagianDatagrid');

        Route::post('/form-add',  'Admin\BagianController@form')->name('formBagian');

        Route::post('/add',  'Admin\BagianController@add')->name('addBagian');

        Route::post('/show',  'Admin\BagianController@show')->name('showBagian');

        Route::post('/delete',  'Admin\BagianController@delete')->name('deleteBagian');

      });     



      Route::group(['prefix'=>'pengaturan'],function(){

        Route::get('/','Admin\PengaturanController@main')->name('Pengaturan');

        Route::post('/simpan','Admin\PengaturanController@simpan')->name('changeIdentity');

       

      });      



    });

  });

 

});

